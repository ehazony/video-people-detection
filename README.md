#################################### <br />
Project Description: <br />
In this project I explore methods to detect the different people that appear in a video.
For each person we will save a single image in the output directory. <br />
#################################### <br />
<br />
#################################### <br />
Solution Method: <br />
Using OpenCV and face-recognition,
1. The algorithm will detect the faces in each frame (using CNN or Hog detection). 
2. Create 128 bit encoding of each face detected. 
3. In order to keep track of the different people recognized, each person will have a cluster of encodings that are detected
   to represent his face.
4. For each new face encoding:
   1. If encoding distance is CLUSTER_LOW_THRESHOLD < distance <= CLUSTER_HIGH_THRESHOLD then encoding is different enough
   from the other encoding representations to add information about the face and close enough for be sure that the encoding 
   represents the same face - so we will add it to the cluster of representations.
   2. If encoding distance is distance <= SIMILARITY_THRESHOLD Then we recognize a face we already know and the representation 
   does not add any new information.
   3. Else distance is large for the other representations - this is a new face so create a new cluster.
<br />
#################################### <br />
<br />
#################################### <br />
Run The Code: <br />
pip install -r requirements.txt <br />
python find_faces.py --input videos/poker.mp4 <br />
python find_faces.py --input videos/poker.mp4 --detection-method cnn <br />
#################################### <br />