import face_recognition
import argparse
import imutils
import cv2
import os
import shutil

OUTPUT_DIR = "result"
SIMILARITY_THRESHOLD = 0.6
CLUSTER_LOW_THRESHOLD = 0.35
CLUSTER_HIGH_THRESHOLD = 0.5


def min_face_match(known_face_encodings, face_encoding_to_check):
    # if no encodings in face_encoding_to_check
    if len(known_face_encodings) == 0:
        return None, None
    scores = face_recognition.face_distance(known_face_encodings, face_encoding_to_check)
    max_index = scores.argmin()
    return max_index, scores[max_index]


def save_new_profile(rgb, box, name_counter):
    top, right, bottom, left = box
    face_img = rgb[top:bottom, left:right, :]
    print("[INFO] saving face image number {}...".format(name_counter))
    cv2.imwrite("{}/images/{}.png".format(OUTPUT_DIR, name_counter), face_img)


def output_data_to_file(ids_to_frame_boxes, dir):
    output_file = "{}/profile_info.txt".format(dir)
    f = open(output_file, 'w+')
    print("[INFO] profiles information is saved at: {}...".format(output_file))
    for id, frames_boxes in ids_to_frame_boxes.items():
        f.write("id: " + str(id))
        f.write("\n")
        f.write("\n".join(str(pair) for pair in frames_boxes))
        f.write("\n-----------------------\n")


if __name__ == "__main__":

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", required=True,
                    help="path to input video")
    ap.add_argument("-d", "--detection-method", type=str, default="hog",
                    help="face detection model to use: either `hog` or `cnn`")
    args = vars(ap.parse_args())

    if not os.path.exists(args["input"]):
        print("[INFO] video path does not exist...")
        exit(1)

    if os.path.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)
    os.mkdir(OUTPUT_DIR)
    os.mkdir(OUTPUT_DIR + "/images")
    print("[INFO] created directory {}...".format(OUTPUT_DIR))

    # initialize the pointer to the video file and the video writer
    print("[INFO] processing video...")
    stream = cv2.VideoCapture(args["input"])

    encoded_faces = []
    encoded_to_id = {}
    ids_to_frame_boxes = {}
    frame_counter = 0
    last_frame_encodings = []
    id_counter = 0

    while True:
        # grab the next frame
        (grabbed, frame) = stream.read()
        if not grabbed:
            break
        # convert the input frame from BGR to RGB then resize it
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if 750 < rgb.shape[0]:
            rgb = imutils.resize(frame, height=750)
        if 750 < rgb.shape[1]:
            rgb = imutils.resize(frame, width=750)

        # find boxes of faces using hog or pre-trained cnn
        boxes = face_recognition.face_locations(rgb,
                                                model=args["detection_method"])
        frame_encodings = face_recognition.face_encodings(rgb, boxes)
        if len(frame_encodings) == 0:
            continue

        for cur_encoding, box in zip(frame_encodings, boxes):
            min_index, min_score = min_face_match(encoded_faces, cur_encoding)
            # check to see if we have found a match
            if min_index is not None and CLUSTER_LOW_THRESHOLD < min_score <= CLUSTER_HIGH_THRESHOLD:
                # found match that is not so close to match encoding.
                # we want to add it in order to create a cluster of representatives for a given face
                match_id = encoded_to_id[encoded_faces[min_index].tostring()]
                encoded_faces.append(cur_encoding)
                encoded_to_id[cur_encoding.tostring()] = match_id
                ids_to_frame_boxes[match_id].append((frame_counter, box))

            elif min_index is not None and min_score <= SIMILARITY_THRESHOLD:
                # found a match that is ether very close to encoding and won't add information
                # or to far from encoding to be positive that the matching is correct
                # only add frame to are faces list
                encoding_id = encoded_to_id[encoded_faces[min_index].tostring()]
                ids_to_frame_boxes[encoding_id].append((frame_counter, box))
            else:
                # is not one of the faces we have seen, open new profile
                encoded_faces.append(cur_encoding)
                encoded_to_id[cur_encoding.tostring()] = id_counter
                ids_to_frame_boxes[id_counter] = [(frame_counter, box)]
                save_new_profile(rgb, box, id_counter)
                id_counter += 1
        frame_counter += 1

    output_data_to_file(ids_to_frame_boxes, OUTPUT_DIR)
